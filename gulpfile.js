// Gulpfile modules
const 	gulp = require('gulp'),
		htmlmin = require('gulp-htmlmin'),
		imagemin = require('gulp-imagemin'),
		sass = require('gulp-sass'),
		del = require('del'),
		runSequence = require('run-sequence'),
		injectSVG = require('gulp-inject-svg'),
		autoprefixer = require('gulp-autoprefixer'),
		sourcemaps = require('gulp-sourcemaps'),
		rev = require('gulp-rev'),
		revReplace = require('gulp-rev-replace'),
		gzip = require('gulp-gzip'),
		prompt = require('gulp-prompt'),
		rsync = require('gulp-rsync'),
		fileInclude = require('gulp-file-include'),
		cssNano = require('gulp-cssnano'),
		fs = require('fs');


const 	browserSync = require('browser-sync').create();
const 	reload = browserSync.reload;

const 	argv = require('minimist')(process.argv);

// Path variables
const 	path_src = 'src/',
		path_dist = 'dist/',
		path_img = 'images/',
		path_styles = 'styles/',
		path_fonts = 'fonts/';

gulp.task('clean-prior', del.bind(null, [path_dist]));

gulp.task('html', () => {
	return gulp.src(path_src + '**/*.html')
	.pipe(injectSVG({base: path_src}))
	.pipe(fileInclude())
	.pipe(htmlmin({
		collapseWhitespace: true,
		minifyCSS: true,
		minifyJS: true
	}))
	.pipe(gulp.dest(path_dist));
});

gulp.task('images', () => {
	return gulp.src(path_src + '**/*.{jpg,png,gif,svg}')
	.pipe(imagemin())
	.pipe(gulp.dest(path_dist));
});

gulp.task('styles', () => {
	return gulp.src(path_src + path_styles + '**/*.scss')
	.pipe(sourcemaps.init())
	.pipe(sass({
		outputStyle: 'compressed'
	})).on('error', sass.logError)
	.pipe(autoprefixer())
	.pipe(cssNano())
	.pipe(sourcemaps.write())
	.pipe(gulp.dest(path_dist + path_styles))
	.pipe(reload({stream: true}));
});

gulp.task('fonts', () => {
	return gulp.src(path_src + path_fonts + '**/*.{woff,woff2}')
	.pipe(gulp.dest(path_dist + path_fonts));
});

gulp.task('revisions', () => {
	return gulp.src(path_dist + path_styles + '**/*.css')
	.pipe(rev())
	.pipe(gulp.dest(path_dist + path_styles))
	.pipe(rev.manifest())
	.pipe(gulp.dest(path_dist + path_styles));
});

gulp.task('replace', () => {
	var manifest = gulp.src(path_dist + path_styles + 'rev-manifest.json')
	return gulp.src(path_dist + '**/*.html')
	.pipe(revReplace({manifest: manifest}))
	.pipe(gulp.dest(path_dist));
});

gulp.task('rev-replace', gulp.series('revisions', 'replace'));

gulp.task('gzip', () => {
	return gulp.src(path_dist + path_styles + '**/*.css')
	.pipe(gzip({append: true}))
	.pipe(gulp.dest(path_dist + path_styles));
});

gulp.task('clean-after', del.bind(null, [
	path_dist + path_styles + 'rev-manifest.json',
	path_dist + path_styles + '!(*-*).?(css|css.gz)'
]));

// Build tasks
gulp.task('build-sequence:dist', gulp.series('html', 'images', 'styles', 'fonts', 'rev-replace', 'gzip', 'clean-after'), () => {
});

gulp.task('build-sequence', gulp.series('html', 'images', 'styles', 'fonts'), () => {
});

gulp.task('build:dist', gulp.series('build-sequence:dist'), () => {
});

gulp.task('build', gulp.series('clean-prior', 'build:dist'), () => {
});

gulp.task('build:test', gulp.series('clean-prior', 'build-sequence'), () => {
});

// Misc tasks
gulp.task('default', gulp.series('build'), () => {
});

// Serve tasks
gulp.task('serve', (done) => {
	browserSync.init({
		notify: false,
		open: false,
		port: 9002,
		server: {
			baseDir: path_dist
		}
	});

	done();

	gulp.watch([
		path_src + '**/*.html',
		path_src + path_img,
		path_src + path_fonts
	]).on('change', reload);

	gulp.watch(path_src + '**/*.html', gulp.series('html'));
	gulp.watch(path_src + path_img + '**/*', gulp.series('images'));
	gulp.watch(path_src + path_styles + '**/*.{css,scss}', gulp.series('styles'));
	gulp.watch(path_src + path_fonts + '**/*.{woff,woff2}', gulp.series('fonts'));
});

gulp.task('serve:dist', () => {
	browserSync.init({
		notify: false,
		open: false,
		port: 9003,
		server: {
			baseDir: path_dist
		}
	});
});

gulp.task('build-serve', gulp.series('build', 'serve'));
gulp.task('build-serve:dist', gulp.series('build:dist', 'serve:dist'));

// Deploy tasks
rsyncPaths = [path_dist];

gulp.task('deploy', () => {
	var credentials = JSON.parse(fs.readFileSync('rsync-credentials.json', 'utf-8'));
	return gulp.src(rsyncPaths)
	.pipe(prompt.confirm({
		message: 'You are currently on the PRODUCTION branch. Push to ' + credentials.destination + '?',
		default: false
	}))
	.pipe(rsync({
		root: path_dist,
		hostname: credentials.hostname,
		username: credentials.username,
		destination: credentials.destination,
		progress: true,
		incremental: true,
		relative: true,
		emptyDirectories: true,
		recursive: true,
		clean: true,
		exclude: ['google*.html', 'favicon.ico', 'robots.txt']
	}));
});

gulp.task('build-deploy', gulp.series('build:dist', 'deploy'));